import {
  StyleSheet,
  Text,
  View,
  Image,
  Pressable,
  StatusBar,
  FlatList,
} from 'react-native';
import React from 'react';
import {connect} from 'react-redux';

import FeedHeader from '../components/FeedHeader';
import PostView from '../components/PostView';

const FeedScreen = ({navigation, posts}) => {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <FeedHeader navigation={navigation} />
      <FlatList
        ListEmptyComponent={<Text style={styles.empty}>No post to show</Text>}
        contentContainerStyle={{flexDirection: 'column-reverse'}}
        data={posts}
        keyExtractor={item => item.postId}
        renderItem={({item}) => (
          <PostView post={item} navigation={navigation} />
        )}
      />
    </View>
  );
};

const mapStateToProps = state => {
  return {posts: state};
};

export default connect(mapStateToProps)(FeedScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e3e3e3',
  },
  empty: {
    alignSelf: 'center',
    marginTop: 50,
    fontSize: 25,
  },
});
