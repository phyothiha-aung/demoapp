import {
  FlatList,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
  Pressable,
} from 'react-native';
import React, {useState} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import {createPost, updatePost} from '../redux/post/postAction';
import MyButton from '../components/MyButton';

const PostActivityScreen = ({navigation, createPost, updatePost, route}) => {
  const [text, setText] = useState(route.params?.post?.bodyText || '');
  const [images, setImages] = useState(route.params?.post?.images || []);

  const selectImage = async () => {
    try {
      await launchImageLibrary(
        {
          mediaType: 'photo',
          quality: 0.5,
        },
        response => {
          if (response.didCancel) return;
          else if (response.errorCode)
            console.warn('error code', response.errorCode);
          else if (response.errorMessage)
            console.warn('error message', response.errorMessage);
          else {
            setImages([...images, response.assets[0].uri]);
          }
        },
      );
    } catch (err) {
      console.warn('error', err);
    }
  };

  const shootImage = async () => {
    try {
      await launchCamera(
        {
          saveToPhotos: true,
          mediaType: 'photo',
          quality: 0.5,
        },
        response => {
          if (response.didCancel) return;
          else if (response.errorCode)
            console.warn('error code', response.errorCode);
          else if (response.errorMessage)
            console.warn('error message', response.errorMessage);
          else {
            setImages([...images, response.assets[0].uri]);
          }
        },
      );
    } catch (err) {
      console.warn('error', err);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <View style={styles.headerBar}>
        <MaterialCommunityIcons
          style={styles.leftItemIcon}
          name="arrow-left"
          size={25}
          onPress={() => navigation.goBack()}
        />

        <Text style={styles.headerTitle}>{route.params?.title}</Text>
        <MyButton
          disabled={!(text.length || images.length)}
          title={route.params?.action}
          onPress={() => {
            if (route.params?.action === 'Post') {
              createPost({
                postId: Math.random().toString(),
                bodyText: text,
                images,
              });
            } else {
              updatePost({
                postId: route.params.post.postId,
                bodyText: text,
                images,
              });
            }

            navigation.goBack();
          }}
        />
      </View>
      <TextInput
        multiline
        placeholder="What's on your mind"
        placeholderTextColor="#aaa"
        fontSize={20}
        value={text}
        onChangeText={t => setText(t)}
      />
      <FlatList
        numColumns={2}
        data={images}
        keyExtractor={item => item}
        renderItem={({item}) => (
          <Pressable
            style={{
              width: '50%',
              height: 200,
              padding: 5,
              backgroundColor: '#e3e3e3',
            }}
            onPress={() => {
              const newImages = images.filter(image => image !== item);
              setImages(newImages);
            }}>
            <Image
              source={{uri: item}}
              style={{width: '100%', height: '100%'}}
              resizeMode="contain"
            />
          </Pressable>
        )}
      />
      <View style={styles.btnGroup}>
        <MyButton
          title="Camera"
          onPress={shootImage}
          icon="camera-outline"
          buttonStyle={{width: 120}}
        />
        <MyButton
          title="Gallery"
          onPress={selectImage}
          icon="view-gallery-outline"
          buttonStyle={{width: 120}}
        />
      </View>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  createPost: post => dispatch(createPost(post)),
  updatePost: post => dispatch(updatePost(post)),
});

export default connect(null, mapDispatchToProps)(PostActivityScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  leftItemIcon: {
    color: 'dodgerblue',
  },
  headerTitle: {
    color: 'dodgerblue',
    marginLeft: 5,
    fontSize: 17,
  },
  btnGroup: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 5,
  },
});
