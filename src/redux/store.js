import {createStore} from 'redux';
import postReducer from './post/postReducer';

const configureStore = () => {
  return createStore(postReducer);
};
export default configureStore;
