import {DELETE, CREATE, UPDATE} from './postActionType';

const initialState = [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE: {
      const newState = state.filter(post => post.postId !== action.payload);
      return newState;
    }
    case CREATE: {
      return [...state, action.payload];
    }
    case UPDATE: {
      const newState = state.map(post =>
        post.postId === action.payload.postId ? action.payload : post,
      );
      return newState;
    }
    default:
      return state;
  }
};

export default reducer;
