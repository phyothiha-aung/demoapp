import {DELETE, CREATE, UPDATE} from './postActionType';

export const deletePost = postId => {
  return {
    type: DELETE,
    payload: postId,
  };
};

export const createPost = post => {
  return {
    type: CREATE,
    payload: post,
  };
};

export const updatePost = post => {
  return {
    type: UPDATE,
    payload: post,
  };
};
