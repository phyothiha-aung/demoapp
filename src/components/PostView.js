import {
  Image,
  Modal,
  Pressable,
  StyleSheet,
  FlatList,
  Text,
  View,
  Alert,
  Dimensions,
} from 'react-native';
import React, {useState} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';

import {deletePost} from '../redux/post/postAction';
import UserComponent from './UserComponent';

const PostView = ({navigation, deletePost, post}) => {
  const [modalVisible, setModalVisible] = useState(false);
  const handleDelete = () => {
    Alert.alert('Delete Post', 'Are you sure you want to delete?', [
      {text: 'Cancle', style: 'cancel'},
      {
        text: 'Delete',
        style: 'destructive',
        onPress: () => deletePost(post.postId),
      },
    ]);
  };
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <UserComponent />
        <MaterialCommunityIcons
          size={25}
          name="chevron-down"
          onPress={() => setModalVisible(true)}
        />
      </View>
      <View style={styles.body}>
        <Text style={styles.bodyText}>{post.bodyText}</Text>
        <View style={styles.imagesContainer}>
          <FlatList
            horizontal
            data={post.images}
            keyExtractor={item => item}
            renderItem={({item}) => {
              return (
                <View
                  style={{width: Dimensions.get('window').width, padding: 5}}>
                  <Image
                    source={{uri: item}}
                    style={{
                      height: 300,
                      width: '100%',
                      alignSelf: 'center',
                    }}
                    resizeMode="contain"
                  />
                </View>
              );
            }}
          />
        </View>
      </View>
      <View style={styles.footer}>
        <View>
          <Text>Like</Text>
        </View>
        <View>
          <Text>Comments</Text>
        </View>
      </View>

      <Modal visible={modalVisible} transparent animationType="slide">
        <Pressable
          style={styles.modalContainer}
          onPress={() => setModalVisible(false)}>
          <View style={styles.modalItems}>
            <Pressable
              style={styles.option}
              onPress={() => {
                handleDelete();
                setModalVisible(false);
              }}>
              <MaterialCommunityIcons
                name="trash-can-outline"
                color="red"
                size={20}
              />
              <Text style={styles.delete}>Delete Post</Text>
            </Pressable>
            <View width="100%" height={1} backgroundColor="#e3e3e3" />
            <Pressable
              style={styles.option}
              onPress={() => {
                setModalVisible(false);
                navigation.navigate('PostActivity', {
                  title: 'Edit Post',
                  action: 'Update',
                  post,
                });
              }}>
              <MaterialCommunityIcons
                name="square-edit-outline"
                color="dodgerblue"
                size={20}
              />
              <Text style={styles.edit}>Edit Post</Text>
            </Pressable>
          </View>
        </Pressable>
      </Modal>
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  deletePost: postId => dispatch(deletePost(postId)),
});

export default connect(null, mapDispatchToProps)(PostView);

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3',
  },
  body: {
    paddingVertical: 10,
  },
  bodyText: {
    fontSize: 17,
    color: '#000',
  },
  footer: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: '#e3e3e3',
  },
  modalContainer: {
    flex: 1,
  },
  modalItems: {
    width: '100%',
    height: '25%',
    marginTop: 'auto',
    backgroundColor: '#ccc',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 20,
  },
  option: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
  },
  delete: {
    fontSize: 17,
    color: 'red',
    marginLeft: 10,
  },
  edit: {
    fontSize: 17,
    color: 'dodgerblue',
    marginLeft: 10,
  },
});
