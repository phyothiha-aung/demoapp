import {StyleSheet, Text, View, Pressable, Image} from 'react-native';
import React from 'react';

const UserComponent = () => {
  return (
    <View style={styles.container}>
      <Pressable>
        <Image
          style={styles.avatar}
          source={require('../assets/images/profile.png')}
        />
      </Pressable>
      <Pressable>
        <Text style={styles.name}>Leia Organa</Text>
        <Text style={styles.userName}>@leiaorgana</Text>
      </Pressable>
    </View>
  );
};

export default UserComponent;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  name: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#000',
  },
  userName: {
    fontSize: 13,
  },
});
