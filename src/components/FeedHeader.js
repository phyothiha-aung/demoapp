import {StyleSheet, Text, View, Pressable, Image} from 'react-native';
import React from 'react';

const FeedHeader = ({navigation}) => {
  return (
    <View style={styles.headerContainer}>
      <Image
        style={styles.avatar}
        source={require('../assets/images/profile.png')}
      />
      <Pressable
        style={({pressed}) => [
          styles.createPostButton,
          {backgroundColor: pressed ? '#e3e3e3' : '#fff'},
        ]}
        onPress={() =>
          navigation.navigate('PostActivity', {
            title: 'Create Post',
            action: 'Post',
          })
        }>
        <Text style={styles.createPostText}>What's on your mind?</Text>
      </Pressable>
    </View>
  );
};

export default FeedHeader;

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3',
    paddingHorizontal: 20,
    paddingVertical: 10,
    elevation: 3,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  createPostButton: {
    width: '80%',
    height: 40,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#e3e3e3',
    paddingHorizontal: 20,
    borderRadius: 20,
  },
  createPostText: {
    color: '#000',
  },
});
