import React from 'react';
import {Text, StyleSheet, Pressable} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const MyButton = ({
  title,
  buttonStyle,
  textStyle,
  disabled = false,
  icon,
  ...rest
}) => {
  return (
    <Pressable
      style={[styles.button, buttonStyle, disabled && styles.disabled]}
      disabled={disabled}
      {...rest}>
      {icon && (
        <MaterialCommunityIcons style={styles.icon} name={icon} size={25} />
      )}
      <Text style={[styles.text, textStyle]}>{title}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 100,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: 'dodgerblue',
  },
  icon: {
    color: '#fff',
    marginRight: 5,
  },
  text: {
    fontSize: 17,
    color: 'white',
  },
  disabled: {
    backgroundColor: 'lightblue',
  },
});

export default MyButton;
