import {StyleSheet, SafeAreaView, PermissionsAndroid} from 'react-native';
import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';

import configureStore from './src/redux/store';
import FeedScreen from './src/screens/FeedScreen';
import PostActivityScreen from './src/screens/PostActivityScreen';

const getPermission = async () => {
  const granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.CAMERA,
  );
  if (granted !== PermissionsAndroid.RESULTS.GRANTED)
    alert('You need to allow permission to use this app');
};

const RootStack = createStackNavigator();
const store = configureStore();
const App = () => {
  useEffect(() => {
    getPermission();
  }, []);
  return (
    <Provider store={store}>
      <SafeAreaView style={styles.container}>
        <NavigationContainer>
          <RootStack.Navigator screenOptions={{headerShown: false}}>
            <RootStack.Group>
              <RootStack.Screen name="Feed" component={FeedScreen} />
            </RootStack.Group>
            <RootStack.Group screenOptions={{presentation: 'modal'}}>
              <RootStack.Screen
                name="PostActivity"
                component={PostActivityScreen}
              />
            </RootStack.Group>
          </RootStack.Navigator>
        </NavigationContainer>
      </SafeAreaView>
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
